# GoogleMusic.bundle

### A Plex plugin for Google Music. An All Access account is not required but many of the features will not work without one.


#### I have also implimented the changes mentioned in [this post](https://github.com/jwdempsey/GoogleMusic.bundle/issues/12#issuecomment-211016373).

Installation Instructions
====

1. Change to your plugin directory
 * __Mac:__ $ `cd ~/Library/Application Support/Plex Media Server/Plug-ins`
 * __Windows:__ C:\> `cd C:\Users\\[user]\AppData\Local\Plex Media Server\Plug-ins`
 * __Linux:__ $ `cd /var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins`
 * __FreeBSD:__ $ `cd /usr/local/plexdata[-plexpass]/Plex Media Server/Plug-ins`
    
2. Execute the following as the user that runs Plex (usually plex for *nix): $ `git clone https://gitlab.com/sisfs/G_Music.git GoogleMusic.bundle`
 * this will create the GoogleMusic.bundle directory and clone the G_Music repo into it.

3. Restart plex: `service plexmediaserver_plexpass restart` on FreeBSD
 * _Please refer to your OS' documentation for assistance_
